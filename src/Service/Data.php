<?php

namespace App\Service;

use App\Entity\Project;
use Doctrine\ORM\EntityManagerInterface;

class Data
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var SerializerService */
    private $serializer;

    /** @var array */
    private $classes;

    /**
     * @param EntityManagerInterface $em
     * @param SerializerService $serializerService
     */
    public function __construct(EntityManagerInterface $em, SerializerService $serializerService)
    {
        $this->em = $em;
        $this->serializer = $serializerService;
        $this->classes = [
            'project' => Project::class,
        ];
    }

    public function getAllDataByType(string $type): array
    {
        return $this->em->getRepository($this->classes[$type])->findAll();
    }

    public function getAllDataSerializeByType(string $type, string $format): string
    {
        return $this->serializer->serialize(
            $this->getAllDataByType($type),
            $format
        );
    }
}