<?php

namespace App\Service;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class SerializerService
{
    private $encoders;
    private $normalizers;
    private $serializer;

    public function __construct()
    {
        $this->encoders = [new XmlEncoder(), new JsonEncoder()];
        $this->normalizers = [new ObjectNormalizer()];
    }

    /**
     * @param array $data
     * @param string $format
     * @return string
     */
    public function serialize(array $data, string $format = 'json'): string
    {
        $this->serializer = new Serializer($this->normalizers, $this->encoders);
        return $this->serializer->serialize($data, $format);
    }
}