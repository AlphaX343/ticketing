<?php

namespace App\Controller;

use App\Entity\Project;
use App\Service\Data;
use App\Service\SerializerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/json")
 */
class JsonController extends AbstractController
{
    /**
     * @Route("", name="json")
     */
    public function index(Request $request, Data $data): Response
    {
        return new Response(
            $data->getAllDataSerializeByType($request->query->get('type'), 'json')
        );
    }
}
