const $ = require('jquery');

require('bootstrap');

import './styles/app.css';
import './styles/global.scss';
import './bootstrap';

import Vue from 'vue';
import Project from "./components/Project";

new Vue({el: '#app', components: {Project}});