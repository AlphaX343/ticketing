<?php

namespace App\Tests\Service\SerializerService;

use App\Service\SerializerService;
use PHPUnit\Framework\TestCase;

class SerializeTest extends TestCase
{
    public function testSerialize(): void
    {
        $serializerService = new SerializerService();
        $this->assertEquals('{"test":"a"}', $serializerService->serialize(['test' => 'a']));
    }
}
